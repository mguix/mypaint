package org.academiadecodigo.bootcamp40;

import org.academiadecodigo.bootcamp40.Cell.Cell;
import org.academiadecodigo.bootcamp40.Cell.MyColorPalette;
import org.academiadecodigo.bootcamp40.Cell.Pointer;
import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;

import java.util.ArrayList;
import java.util.List;

public class Grid {

    private static final int PADDING = 10;
    private static final int CELL_SIZE = 15;

    private Rectangle grid;
    private Pointer pointer;
    private MyColorPalette palette;
    private List<Cell> cells = new ArrayList<>();

    void init(int gridSize) {

        grid = new Rectangle(PADDING, PADDING, gridSize * CELL_SIZE, gridSize * CELL_SIZE);
        grid.setColor(Color.LIGHT_GRAY);
        grid.draw();
        palette = new MyColorPalette();

        cellCreator(gridSize, gridSize);
        pointer = new Pointer(PADDING, CELL_SIZE);
    }

    private void cellCreator(int width, int height) {

        for (int col = PADDING+CELL_SIZE; col < width * CELL_SIZE + PADDING; col += CELL_SIZE) {

            for (int row = PADDING; row < height * CELL_SIZE + PADDING; row += CELL_SIZE) {

                Cell cell = new Cell(col, row, CELL_SIZE);
                cells.add(cell);
                cell.show();
            }
        }
    }

    void paint() {

        for (Cell c : cells) {

            if (c.equals(pointer) && palette.equalsColor(pointer)) {
                c.setChosenColor(palette.getPickedColor());
                return;
            }

            if (c.equals(pointer)){

                if (c.isPainted()) {
                    c.clear();
                    return;
                }

                c.paint();
                return;
            }
        }
    }

    void movePointer(Pointer.Directions directions) {


        switch (directions) {

            case RIGHT:
                if (pointer.getX() < grid.getWidth()) {
                    pointer.moveRight();
                }
                break;

            case LEFT:
                if (pointer.getX() > PADDING) {
                    pointer.moveLeft();
                }
                break;

            case UP:
                if (pointer.getY() > PADDING) {
                    pointer.moveUp();
                }
                break;

            case DOWN:
                if (pointer.getY() < grid.getHeight()) {
                    pointer.moveDown();
                }
                break;
        }
    }

    public static int getCellSize() {
        return CELL_SIZE;
    }

    public static int getPadding(){
        return PADDING;
    }

    public void save() {
        FileManager.writeToFile(toString());
    }

    public void load() {
        stringToGrid(FileManager.readFile());
    }

    public void clearAll() {

        for (Cell c : cells) {
            c.clear();
        }
    }

    @Override
    public String toString() {

        StringBuilder stringBuilder = new StringBuilder();

        for (Cell c : cells) {

            stringBuilder.append(c);
        }

        return stringBuilder.toString();
    }

    public void stringToGrid(String savedFile) {

        String[] cellsArray = savedFile.split("");

        for (int i = 0; i < this.cells.size(); i++) {
            Cell cell = this.cells.get(i);

            if (cellsArray[i].equals("0")) {
                cell.clear();
                continue;
            }
            cell.paint();
        }
    }
}
