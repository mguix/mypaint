package org.academiadecodigo.bootcamp40;

public class Main {

    /*

    1. Draw Canvas - DONE
    2. Create grid/cells - DONE
    3. create pointer - DONE
    4. Create input handler
    5. Move pointer
    6. Paint
    7. Toggle cell
    8. Save to file
    9. Load from file
    10. Quit


     */

    public static void main(String[]args){

       Grid grid = new Grid();
       new KeyboardController(grid);

       grid.init(40);

    }
}
