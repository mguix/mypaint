package org.academiadecodigo.bootcamp40;

import org.academiadecodigo.bootcamp40.Cell.Pointer;
import org.academiadecodigo.simplegraphics.keyboard.Keyboard;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEventType;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardHandler;


public class KeyboardController implements KeyboardHandler {

    private Grid grid;
    private Keyboard keyboard;

    public KeyboardController(Grid grid) {
        this.grid = grid;
        keyboard = new Keyboard(this);
        keyboardInit();
    }

    public void keyboardInit() {

        KeyboardEvent rightPressed = new KeyboardEvent();
        rightPressed.setKey(KeyboardEvent.KEY_RIGHT);
        rightPressed.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);

        KeyboardEvent leftPressed = new KeyboardEvent();
        leftPressed.setKey(KeyboardEvent.KEY_LEFT);
        leftPressed.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);

        KeyboardEvent upPressed = new KeyboardEvent();
        upPressed.setKey(KeyboardEvent.KEY_UP);
        upPressed.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);

        KeyboardEvent downPressed = new KeyboardEvent();
        downPressed.setKey(KeyboardEvent.KEY_DOWN);
        downPressed.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);

        KeyboardEvent spacePressed = new KeyboardEvent();
        spacePressed.setKey(KeyboardEvent.KEY_SPACE);
        spacePressed.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);

        KeyboardEvent sPressed = new KeyboardEvent();
        sPressed.setKey(KeyboardEvent.KEY_S);
        sPressed.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);

        KeyboardEvent lPressed = new KeyboardEvent();
        lPressed.setKey(KeyboardEvent.KEY_L);
        lPressed.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);

        KeyboardEvent cPressed = new KeyboardEvent();
        cPressed.setKey(KeyboardEvent.KEY_C);
        cPressed.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);

        keyboard.addEventListener(downPressed);
        keyboard.addEventListener(upPressed);
        keyboard.addEventListener(leftPressed);
        keyboard.addEventListener(rightPressed);
        keyboard.addEventListener(spacePressed);
        keyboard.addEventListener(sPressed);
        keyboard.addEventListener(lPressed);
        keyboard.addEventListener(cPressed);
    }

    @Override
    public void keyPressed(KeyboardEvent keyboardEvent) {

        switch (keyboardEvent.getKey()) {

            case KeyboardEvent.KEY_RIGHT:
                grid.movePointer(Pointer.Directions.RIGHT);
                break;

            case KeyboardEvent.KEY_LEFT:
                grid.movePointer(Pointer.Directions.LEFT);
                break;

            case KeyboardEvent.KEY_UP:
                grid.movePointer(Pointer.Directions.UP);
                break;

            case KeyboardEvent.KEY_DOWN:
                grid.movePointer(Pointer.Directions.DOWN);
                break;

            case KeyboardEvent.KEY_SPACE:
                grid.paint();
                break;

            case KeyboardEvent.KEY_S:
                grid.save();
                break;

            case KeyboardEvent.KEY_L:
                grid.load();
                break;
            case KeyboardEvent.KEY_C:
                grid.clearAll();
                break;

            default:
                break;
        }
    }

    @Override
    public void keyReleased(KeyboardEvent keyboardEvent) {
    }
}
