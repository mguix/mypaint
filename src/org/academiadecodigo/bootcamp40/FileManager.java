package org.academiadecodigo.bootcamp40;

import java.io.*;

public class FileManager {

    private static final String FILEPATH = "resources/paintFile.txt";

    public static void writeToFile(String string){

        BufferedWriter writer;

        try{

            writer = new BufferedWriter(new FileWriter(FILEPATH));
            writer.write(string);
            writer.close();

        } catch (IOException ex){
            System.out.println(ex.getMessage());
        }
    }

    public static String readFile(){

        String result = "";
        BufferedReader reader;

        try {

            reader = new BufferedReader(new FileReader(FILEPATH));
            result = reader.readLine();

        } catch (FileNotFoundException ex){
            System.out.println(ex.getMessage());

        } catch (IOException ex){
            System.out.println(ex.getMessage());
        }
        return result;
    }
}
