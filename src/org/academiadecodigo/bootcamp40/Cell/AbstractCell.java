package org.academiadecodigo.bootcamp40.Cell;

import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;

public class AbstractCell {

    Rectangle cell;
    int col;
    int row;

    AbstractCell(int col, int row, int cellSize) {
        this.col = col;
        this.row = row;
        cell = new Rectangle(col, row, cellSize, cellSize);
    }

    public void show() {
        cell.setColor(Color.LIGHT_GRAY);
        cell.draw();
    }
}
