package org.academiadecodigo.bootcamp40.Cell;

import org.academiadecodigo.bootcamp40.Grid;
import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;

public class Pointer extends AbstractCell {

    private Rectangle pointer;

    public Pointer(int padding, int cellSize) {
        super(padding, padding, cellSize);

        pointer = new Rectangle(padding+cellSize, padding, cellSize, cellSize);
        pointer.setColor(Color.BLACK);
        pointer.draw();
    }

    public void setPos(int col, int row) {
        this.col = col;
        this.row = row;
    }

    int getCol() {
        return col;
    }

    int getRow() {
        return row;
    }

    public int getX() {
        return pointer.getX();
    }

    public int getY() {
        return pointer.getY();
    }

    public void moveRight() {
        pointer.translate(Grid.getCellSize(), 0);
        setPos(pointer.getX(), pointer.getY());
    }

    public void moveLeft() {
        pointer.translate(-Grid.getCellSize(), 0);
        setPos(pointer.getX(), pointer.getY());
    }

    public void moveUp() {
        pointer.translate(0, -Grid.getCellSize());
        setPos(pointer.getX(), pointer.getY());
    }

    public void moveDown() {
        pointer.translate(0, Grid.getCellSize());
        setPos(pointer.getX(), pointer.getY());
    }


    public enum Directions {

        UP,
        DOWN,
        LEFT,
        RIGHT

    }
}
