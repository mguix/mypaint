package org.academiadecodigo.bootcamp40.Cell;

import org.academiadecodigo.simplegraphics.graphics.Color;

public class Cell extends AbstractCell {


    private boolean painted = false;
    private Color chosenColor;

    public Cell(int col, int row, int cellSize) {
        super(col, row, cellSize);
    }

    public void paint() {
        painted = true;
        cell.setColor(chosenColor);
        cell.fill();
    }

    public void clear() {
        painted = false;
        cell.setColor(Color.LIGHT_GRAY);
        cell.draw();
    }

    public boolean isPainted() {
        return painted;
    }

    public boolean equals(Pointer pointer) {
        return col == pointer.getCol() && row == pointer.getRow();
    }

    public void setChosenColor(Color color){
        chosenColor = color;
    }

    @Override
    public String toString(){
        return painted ? "1" : "0";
    }



}
