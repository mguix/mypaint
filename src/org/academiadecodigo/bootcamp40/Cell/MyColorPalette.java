package org.academiadecodigo.bootcamp40.Cell;

import org.academiadecodigo.bootcamp40.Grid;
import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;

import javax.print.DocFlavor;

public class MyColorPalette {

    protected static final Color PASTEL_YELLOW = new Color(255, 221, 156);
    protected static final Color CLEAR_YELLOW = new Color(254, 213, 34);
    protected static final Color PASTEL_ORANGE = new Color(245, 161, 49);
    protected static final Color CLEAR_ORANGE = new Color(254, 186, 15);
    protected static final Color PASTEL_PINK = new Color(254, 192, 171);
    protected static final Color PINK_CREME = new Color(255, 135, 111);
    protected static final Color SOFT_PINK = new Color(235, 76, 98);
    protected static final Color DARK_PINK = new Color(255, 76, 97);
    protected static final Color SOFT_RED = new Color(203, 51, 64);
    protected static final Color CLEAR_BLUE = new Color(151, 213, 223);
    protected static final Color OCEAN_BLUE = new Color(2, 161, 146);
    protected static final Color ROYAL_BLUE = new Color(94, 125, 243);
    protected static final Color DARK_BLUE = new Color(11, 76, 138);
    protected static final Color PASTEL_PURPLE = new Color(205, 194, 211);
    protected static final Color CLEAR_PURPLE = new Color(157, 119, 154);
    protected static final Color DARK_PURPLE = new Color(109, 90, 151);
    protected static final Color PASTEL_GREEN = new Color(169, 225, 186);
    protected static final Color CLEAR_GREEN = new Color(149, 192, 76);
    protected static final Color TREE_GREEN = new Color(8, 133, 64);
    protected static final Color CLEAR_BROWN = new Color(209, 175, 148);
    protected static final Color DARK_BROWN = new Color(173, 93, 22);


    private Rectangle[] rectangles;
    private Color[] colors;
    private Color pickedColor;


    public MyColorPalette() {
        rectangles = new Rectangle[21];
        colors = new Color[]{
                PASTEL_YELLOW,
                CLEAR_YELLOW,
                PASTEL_ORANGE,
                CLEAR_ORANGE,
                PASTEL_PINK,
                PINK_CREME,
                SOFT_PINK,
                DARK_PINK,
                SOFT_RED,
                CLEAR_BLUE,
                OCEAN_BLUE,
                ROYAL_BLUE,
                DARK_BLUE,
                PASTEL_PURPLE,
                CLEAR_PURPLE,
                DARK_PURPLE,
                PASTEL_GREEN,
                CLEAR_GREEN,
                TREE_GREEN,
                CLEAR_BROWN,
                DARK_BROWN
        };
        setColorPalette();
    }

    public void setColorPalette(){

        for (int color = 0; color < colors.length; color++){

            rectangles[color] = new Rectangle(Grid.getPadding(), Grid.getPadding() + Grid.getCellSize() * color, Grid.getCellSize(), Grid.getCellSize());
            rectangles[color].setColor(colors[color]);
            rectangles[color].fill();
        }
    }

    public boolean equalsColor(Pointer pointer){

        for (int i=0; i < rectangles.length; i++){
            if (rectangles[i].getX() == pointer.getX() && rectangles[i].getY() == pointer.getY()){
              pickedColor = rectangles[i].getColor();
              return true;
            }
        }

        return false;
    }

    public Color getPickedColor(){
        return pickedColor;
    }



}
